<?php
include_once 'Promyka/Email.php';

class ZwAuthBasic
{

    const SESSIONKEY = 'ZwAuthBasicToken';
    const SESSIONKEY_ATTEMPTS = 'ZwAuthBasicAttempts';

    static public function session()
    {
        return ZwApplication::getInstance()->session();
    }

    static function doAuthenticate($text) {
        $attempts = self::session()->get(self::SESSIONKEY_ATTEMPTS);
        if (!$attempts) {
            self::session()->set(self::SESSIONKEY_ATTEMPTS, 1);
            $attempts = 1;
        }

        if ($attempts > 10) {
            echo 'Too many attempts. Close your browser window and try again.';
            return;
        }
        $attempts += 1;
        self::session()->set(self::SESSIONKEY_ATTEMPTS, $attempts);
        header('WWW-Authenticate: Basic realm="'.$text.'"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'You are not authorized to enter secured section.';
    }

    static public function authenticate(callable $verifyuserFn, callable $generateKeyFn, callable $sendKeyFn)
    {
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            self::doAuthenticate('Enter just a user name and leave password empty');
            return null;
        } else {
            if ($verifyuserFn($_SERVER['PHP_AUTH_USER'])) {
                if (!self::session()->get(self::SESSIONKEY)) {
                    $key = $generateKeyFn();
                    $sendKeyFn($key);
                    self::session()->set(self::SESSIONKEY, $key);
                    self::doAuthenticate('Password valid just for this session has been sent to your e-mail. Please enter both your e-mail and password');
                    return null;
                } else if (self::session()->get(self::SESSIONKEY) == $_SERVER['PHP_AUTH_PW']) {
                        return $_SERVER['PHP_AUTH_USER']; //validated
                } else {
                    self::doAuthenticate('Try again');
                    return null;
                }
            } else {
                $text = (self::session()->get(self::SESSIONKEY)) ? 'Try again' : 'Enter just a user name and leave password empty';
                self::doAuthenticate($text);
                return null;
            }
       }
    }
}